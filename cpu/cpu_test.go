package cpu

import (
	"testing"
)

func TestLDA(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0x05, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.acc != 0x05 {
		t.Errorf("ACC incorrect, got: %d want: %d", ncpu.acc, 0x05)
	}
}

func TestLDAZeroFlag(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0x00, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.status&0b00000010 != 0b10 {
		t.Errorf("Zero Flag was not set on LDA")
	}
}

func TestLDANegFlag(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0xFF, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.status&0b10000000 != 0b10000000 {
		t.Errorf("Neg Flag was not set on LDA: %b", ncpu.status)
	}
}

func TestTAX(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0x69, 0xAA, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.reg_x != 0x69 {
		t.Errorf("REG_X incorrect, got: %x want: %x", ncpu.reg_x, 0x69)
	}
}

func TestTAXZeroFlag(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xAA, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.status&0b00000010 != 0b10 {
		t.Errorf("Zero Flag was not set on TAX")
	}
}

func TestTAXNegFlag(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0xFF, 0xAA, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.status&0b10000000 != 0b10000000 {
		t.Errorf("Neg Flag was not set on TAX: %b", ncpu.status)
	}
}

func TestINX(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0x01, 0xAA, 0xE8, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.reg_x != 0x02 {
		t.Errorf("REG_X incorrect, got: %x want: %x", ncpu.reg_x, 0x02)
	}
}

func TestINXOverflow(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0xFF, 0xAA, 0xE8, 0xE8, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.reg_x != 1 {
		t.Errorf("REG_X incorrect, got: %x want: %x", ncpu.reg_x, 0x01)
	}
}

func Test5OpsWorkingTogether(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xa9, 0xc0, 0xaa, 0xe8, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.reg_x != 0xc1 {
		t.Errorf("REG_X incorrect, got: %x want: %x", ncpu.reg_x, 0xc1)
	}
}

func TestSTX(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xE8, 0x86, 0x01}
	ncpu.LoadAndRun(prog)
	if ncpu.MemRead(0x01) != 0x01 {
		t.Errorf("MEM incorrect, got: %x want: %x", ncpu.MemRead(0x01), 0x01)
	}
}

func TestSTA(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0x69, 0x85, 0x42, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.MemRead(0x42) != 0x69 {
		t.Errorf("MEM incorrect, got: %x want: %x", ncpu.MemRead(0x00), 0x69)
	}
}

func TestAND(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0xCF, 0x29, 0xBF, 0x00}
	ncpu.LoadAndRun(prog)
	if ncpu.acc != 0x8F {
		t.Errorf("ACC incorrect, got: %x want: %x", ncpu.acc, 0x8F)
	}
}

func TestASL(t *testing.T) {
	ncpu := NewCPU()
	prog := []uint8{0xA9, 0x02, 0x0A}
	ncpu.LoadAndRun(prog)
	if ncpu.acc != 0x04 {
		t.Errorf("ACC incorrect, got: %x want: %x", ncpu.acc, 0x04)
	}
}

func TestBCC(t *testing.T) {
	prog := []uint8{0x90, 0x04, 0x00, 0xE8, 0xE8, 0xE8, 0xE8}
	ncpu := NewCPU()
	ncpu.LoadAndRun(prog)
	if ncpu.reg_x != 0x02 {
		t.Errorf("REGX incorrect, got: %x want: %x", ncpu.reg_x, 0x02)
	}
}

func TestBCS(t *testing.T) {
	prog := []uint8{0xB0, 0x04, 0x00, 0xE8, 0xE8, 0xE8, 0xE8}
	ncpu := NewCPU()
	ncpu.Load(prog)
	ncpu.SetStatus(0xFF)
	ncpu.Step(100)
	if ncpu.reg_x != 0x02 {
		t.Errorf("REGX incorrect, got: %x want: %x", ncpu.reg_x, 0x02)
	}
}

func TestBEQ(t *testing.T) {
	prog := []uint8{0xF0, 0x04, 0x00, 0xE8, 0xE8, 0xE8, 0xE8}
	ncpu := NewCPU()
	ncpu.Load(prog)
	ncpu.SetStatus(0xFF)
	ncpu.Step(100)
	if ncpu.reg_x != 0x02 {
		t.Errorf("REGX incorrect, got: %x want: %x", ncpu.reg_x, 0x02)
	}
}
