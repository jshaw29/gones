package cpu

// type CPUInstruction func()

//  var ins_map = map[uint8]CPUInstruction{
// 	0xa9:CPU.LDA,
// 	0xAA:TAX,
// 	0xE9:INX,
// }
import (
	"addrmode"
	"fmt"
	"opcode"
)

// type AddressMode int

// const (
// 	Immediate AddressMode = iota
// 	ZeroPage              = iota
// 	ZeroPageX             = iota
// 	ZeroPageY             = iota
// 	Absolute              = iota
// 	AbsoluteX             = iota
// 	AbsoluteY             = iota
// 	IndirectX             = iota
// 	IndirectY             = iota
// 	NA                    = iota
// )

type CPU struct {
	acc           uint8
	reg_x         uint8
	reg_y         uint8
	pc            uint16
	status        uint8
	stack_pointer uint8
	memory        [0xFFFF]uint8
}

func (cpu *CPU) SetStatus(status uint8) {
	cpu.status = status
}

func PrintCPUState(c CPU) {
	fmt.Println("#########################")
	fmt.Printf("ACC: %x \n", c.acc)
	fmt.Printf("REG_X: %x \n", c.reg_x)
	fmt.Printf("REG_Y: %x \n", c.reg_y)
	fmt.Printf("PX: %x \n", c.pc)
	fmt.Printf("STACK POINTER: %x \n", c.stack_pointer)
	fmt.Printf("STATUS: %x \n", c.status)
}

func NewCPU() *CPU {
	cpu := new(CPU)
	cpu.acc = 0
	cpu.reg_x = 0
	cpu.reg_y = 0
	cpu.pc = 0
	cpu.stack_pointer = 0
	cpu.status = 0
	return cpu
}

func (cpu *CPU) execute(opscode *opcode.OPCode) int {
	prgcnt := cpu.pc
	fmt.Println(opscode)
	switch opscode.CodeType {
	case "LDA": // LDA
		cpu.LDA(opscode.Mode)
	case "TAX": //TAX
		cpu.TAX()
	case "INX": //INX
		cpu.INX()
	case "STA": // STA
		cpu.STA(opscode.Mode)
	case "STX": // STX
		cpu.STX(opscode.Mode)
	case "AND":
		cpu.AND(opscode.Mode)
	case "ASL":
		cpu.ASL(opscode.Mode)
	case "BCC":
		cpu.BCC(opscode.Mode)
	case "BCS":
		cpu.BCS(opscode.Mode)
	case "BEQ":
		cpu.BEQ(opscode.Mode)
	case "BRK":
		return 1
	default:
		panic(fmt.Sprintf("Unknown OPCode %x\n", opscode.Code))
	}
	if prgcnt == cpu.pc {
		cpu.pc += uint16(opscode.ByteLen) - 1
	}
	return 0
}

func (cpu *CPU) run() {
	opscodes := opcode.GetOPCodeMap()
	for {
		code := cpu.MemRead(cpu.pc)
		cpu.pc = cpu.pc + 1
		opscode, prs := opscodes[code]
		if !prs {
			panic(fmt.Sprintf("Unknown OPCode %x\n", code))
		}
		// fmt.Println(opscode)
		state := cpu.execute(opscode)
		if state == 1 {
			return
		}
		// fmt.Printf(" %x : %x\n", prgcnt, cpu.pc)
	}
}

func (cpu *CPU) Step(steps int) {
	opscodes := opcode.GetOPCodeMap()
	for i := 0; i < steps; i++ {
		code := cpu.MemRead(cpu.pc)
		cpu.pc = cpu.pc + 1
		opscode, prs := opscodes[code]
		if !prs {
			panic(fmt.Sprintf("Unknown OPCode %x\n", code))
		}
		// fmt.Println(opscode)
		state := cpu.execute(opscode)
		if state == 1 {
			return
		}
	}
}

func (cpu *CPU) GetOpAddr(mode addrmode.AddressMode) uint16 {
	switch mode {
	case addrmode.Immediate:
		return cpu.pc
	case addrmode.ZeroPage:
		return uint16(cpu.MemRead(cpu.pc))
	case addrmode.Absolute:
		return cpu.MemRead16(cpu.pc)
	case addrmode.ZeroPageX:
		stpos := uint16(cpu.MemRead(cpu.pc))
		return stpos + uint16(cpu.reg_x)
	case addrmode.ZeroPageY:
		stpos := uint16(cpu.MemRead(cpu.pc))
		return stpos + uint16(cpu.reg_y)
	case addrmode.AbsoluteX:
		stpos := cpu.MemRead16(cpu.pc)
		return stpos + uint16(cpu.reg_x)
	case addrmode.AbsoluteY:
		stpos := cpu.MemRead16(cpu.pc)
		return stpos + uint16(cpu.reg_y)
	case addrmode.IndirectX:
		stpos := cpu.MemRead(cpu.pc)
		var pointer uint8 = stpos + cpu.reg_x
		low := uint16(cpu.MemRead(uint16(pointer)))
		high := uint16(cpu.MemRead(uint16(pointer) + 1))
		return high<<8 | low
	case addrmode.IndirectY:
		stpos := cpu.MemRead(cpu.pc)
		low := uint16(cpu.MemRead(uint16(stpos)))
		high := uint16(cpu.MemRead(uint16(stpos) + 1))
		addr := high<<8 | low
		return addr + uint16(cpu.reg_y)
	default:
		panic(-1)
	}
}

func (cpu *CPU) Reset() {
	cpu.acc = 0
	cpu.reg_x = 0
	cpu.reg_y = 0
	cpu.pc = cpu.MemRead16(0xFFFC)
	cpu.stack_pointer = 0
	cpu.status = 0
}

func (cpu *CPU) MemRead(address uint16) uint8 {
	return cpu.memory[address]
}

func (cpu *CPU) MemRead16(address uint16) uint16 {
	var low uint16 = uint16(cpu.MemRead(address))
	var high uint16 = uint16(cpu.MemRead(address + 1))
	return (high << 8) | low
}

func (cpu *CPU) MemWrite(address uint16, data uint8) {
	cpu.memory[address] = data
}

func (cpu *CPU) MemWrite16(address uint16, data uint16) {
	var high = uint8((data >> 8))
	var low = uint8(data & 0xFF)
	cpu.MemWrite(address, low)
	cpu.MemWrite(address+1, high)
}

func (cpu *CPU) LoadProgam(program []uint8) {
	j := 0
	for i := 0x8000; i < 0x8000+len(program) && i <= len(cpu.memory); i++ {
		cpu.memory[i] = program[j]
		j++
	}
	cpu.MemWrite16(0xFFFC, 0x8000)
}

func (cpu *CPU) LoadAndRun(program []uint8) {
	cpu.LoadProgam(program)
	cpu.Reset()
	cpu.run()
}

func (cpu *CPU) Load(program []uint8) {
	cpu.LoadProgam(program)
	cpu.Reset()
}

func (cpu *CPU) LDA(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	// fmt.Printf("addr: %x\n", addr)
	val := cpu.MemRead(addr)
	// fmt.Printf("Val %x\n", val)
	cpu.acc = val
	cpu.HandleZeroFlag(cpu.acc)
	cpu.HandleNegFlag(cpu.acc)
}

func (cpu *CPU) TAX() {
	cpu.reg_x = cpu.acc
	cpu.HandleZeroFlag(cpu.reg_x)
	cpu.HandleNegFlag(cpu.reg_x)
}

func (cpu *CPU) INX() {
	cpu.reg_x += 1
	cpu.HandleNegFlag(cpu.reg_x)
	cpu.HandleZeroFlag(cpu.reg_x)
}

func (cpu *CPU) STA(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	cpu.MemWrite(addr, cpu.acc)
}

func (cpu *CPU) STX(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	cpu.MemWrite(addr, cpu.reg_x)
}

func (cpu *CPU) AND(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	val := cpu.MemRead(addr)
	cpu.acc = cpu.acc & val
	cpu.HandleZeroFlag(cpu.acc)
	cpu.HandleNegFlag(cpu.acc)
}

func (cpu *CPU) ASL(mode addrmode.AddressMode) {
	var val uint8 = 0
	var addr uint16 = 0
	if mode != addrmode.NA {
		addr = cpu.GetOpAddr(mode)
		val = cpu.MemRead(addr)
	} else {
		val = cpu.acc
	}
	if val>>7 == 1 {
		cpu.SetCarryFlag()
	} else {
		cpu.ClearCarryFlag()
	}
	val = val << 1
	if mode == addrmode.NA {
		cpu.acc = val
	} else {
		cpu.MemWrite(addr, val)
	}
}

func (cpu *CPU) BCC(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	if cpu.status&0b00000001 == 0x0 {
		cpu.Branch(int8(addr))
	}
}

func (cpu *CPU) BCS(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	if cpu.status&0b00000001 == 0x1 {
		cpu.Branch(int8(addr))
	}
}

func (cpu *CPU) BEQ(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	if cpu.status&0b00000010 == 0b10 {
		cpu.Branch(int8(addr))
	}
}

func (cpu *CPU) BIT(mode addrmode.AddressMode) {
	addr := cpu.GetOpAddr(mode)
	val := cpu.MemRead(addr)
	mask := cpu.acc & val
	cpu.HandleZeroFlag(mask)
	cpu.HandleNegFlag(val)
	cpu.HandleOverflowFlag(val)
}

func (cpu *CPU) HandleZeroFlag(check uint8) {
	if check == 0 {
		cpu.status = cpu.status | 0b00000010
	} else {
		cpu.status = cpu.status & 0b11111101
	}
}

func (cpu *CPU) HandleNegFlag(check uint8) {
	if check&0b1000000 != 0 {
		cpu.status = cpu.status | 0b10000000
	} else {
		cpu.status = cpu.status & 0b01111111
	}
}

func (cpu *CPU) HandleOverflowFlag(check uint8) {
	if check&0b0100000 != 0 {
		cpu.status = cpu.status | 0b01000000
	} else {
		cpu.status = cpu.status & 0b10111111
	}
}

func (cpu *CPU) SetCarryFlag() {
	cpu.status = cpu.status | 0b0000001
}

func (cpu *CPU) ClearCarryFlag() {
	cpu.status = cpu.status & 0b1111110
}

func (cpu *CPU) Branch(offset int8) {
	fmt.Println(offset)
	if offset < 0 {
		cpu.pc -= uint16(uint8(-offset))
	} else {
		cpu.pc += uint16(uint8(offset))
	}
	fmt.Printf("New PC: %x \n", cpu.pc)

}
