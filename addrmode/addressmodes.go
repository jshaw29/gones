package addrmode

type AddressMode int

const (
	Immediate AddressMode = iota
	ZeroPage              = iota
	ZeroPageX             = iota
	ZeroPageY             = iota
	Absolute              = iota
	AbsoluteX             = iota
	AbsoluteY             = iota
	IndirectX             = iota
	IndirectY             = iota
	NA                    = iota
)
