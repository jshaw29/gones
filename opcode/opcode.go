package opcode

import "addrmode"

type OPCode struct {
	Code     uint8
	CodeType string
	ByteLen  uint8
	Cycles   uint8
	Mode     addrmode.AddressMode
}

func NewOPCode(code uint8, CodeType string, byte_len, cycles uint8, mode addrmode.AddressMode) *OPCode {
	op := new(OPCode)
	op.Code = code
	op.CodeType = CodeType
	op.ByteLen = byte_len
	op.Cycles = cycles
	op.Mode = mode
	return op
}

var CodeDefs = []*OPCode{
	NewOPCode(0x00, "BRK", 1, 7, addrmode.NA),
	NewOPCode(0xAA, "TAX", 1, 2, addrmode.NA),
	NewOPCode(0xE8, "INX", 1, 2, addrmode.NA),
	NewOPCode(0xA9, "LDA", 2, 2, addrmode.Immediate),
	NewOPCode(0xA5, "LDA", 2, 3, addrmode.ZeroPage),
	NewOPCode(0xB5, "LDA", 2, 4, addrmode.ZeroPageX),
	NewOPCode(0xAD, "LDA", 3, 4, addrmode.Absolute),
	NewOPCode(0xBD, "LDA", 3, 4 /*+1 if page crossed*/, addrmode.AbsoluteX),
	NewOPCode(0xB9, "LDA", 3, 4 /*+1 if page crossed*/, addrmode.AbsoluteY),
	NewOPCode(0xA1, "LDA", 2, 6, addrmode.IndirectX),
	NewOPCode(0xB1, "LDA", 2, 5 /*+1 if page crossed*/, addrmode.IndirectY),
	NewOPCode(0x85, "STA", 2, 3, addrmode.ZeroPage),
	NewOPCode(0x95, "STA", 2, 4, addrmode.ZeroPageX),
	NewOPCode(0x8D, "STA", 3, 4, addrmode.Absolute),
	NewOPCode(0x9D, "STA", 3, 5, addrmode.AbsoluteX),
	NewOPCode(0x99, "STA", 3, 5, addrmode.AbsoluteY),
	NewOPCode(0x81, "STA", 2, 6, addrmode.IndirectX),
	NewOPCode(0x91, "STA", 2, 6, addrmode.IndirectY),
	NewOPCode(0x86, "STX", 2, 3, addrmode.ZeroPage),
	NewOPCode(0x96, "STX", 2, 4, addrmode.ZeroPageY),
	NewOPCode(0x8E, "STX", 2, 4, addrmode.Absolute),
	NewOPCode(0x29, "AND", 2, 2, addrmode.Immediate),
	NewOPCode(0x25, "AND", 2, 3, addrmode.ZeroPage),
	NewOPCode(0x35, "AND", 2, 4, addrmode.ZeroPageX),
	NewOPCode(0x2D, "AND", 2, 4, addrmode.Absolute),
	NewOPCode(0x3D, "AND", 3, 4 /*+1 if page crossed*/, addrmode.AbsoluteX),
	NewOPCode(0x39, "AND", 3, 4 /*+1 if page crossed*/, addrmode.AbsoluteY),
	NewOPCode(0x21, "AND", 2, 6, addrmode.IndirectX),
	NewOPCode(0x31, "AND", 3, 5 /*+1 if page crossed*/, addrmode.IndirectY),
	NewOPCode(0x0A, "ASL", 1, 2, addrmode.NA),
	NewOPCode(0x06, "ASL", 2, 5, addrmode.ZeroPage),
	NewOPCode(0x16, "ASL", 2, 6, addrmode.ZeroPageX),
	NewOPCode(0x0E, "ASL", 3, 6, addrmode.Absolute),
	NewOPCode(0x1E, "ASL", 3, 7, addrmode.AbsoluteX),
	NewOPCode(0x90, "BCC", 2, 2 /* (+1 if branch succeeds +2 if to a new page)*/, addrmode.Absolute),
	NewOPCode(0xB0, "BCS", 2, 2 /* (+1 if branch succeeds +2 if to a new page)*/, addrmode.Absolute),
	NewOPCode(0xF0, "BEQ", 2, 2 /* (+1 if branch succeeds +2 if to a new page)*/, addrmode.Absolute),
}

func GetOPCodeMap() map[uint8]*OPCode {
	out := make(map[uint8]*OPCode)
	for _, op := range CodeDefs {
		out[op.Code] = op
	}
	return out
}
