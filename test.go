package main

import (
	"cpu"
)

func main() {
	ncpu := cpu.NewCPU()
	prog := []uint8{0xF0, 0x04, 0x00, 0xE8, 0xE8, 0xE8, 0xE8}
	ncpu.Load(prog)
	ncpu.SetStatus(0xFF)
	ncpu.Step(1000)
	cpu.PrintCPUState(*ncpu)
}
